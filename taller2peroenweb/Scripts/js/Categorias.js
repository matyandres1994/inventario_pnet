﻿$(document).ready(function () {
    CargaCategoria();

    CargaProductos();
    $('#btnIngresar').unbind('click').click(function (e) {
        e.preventDefault();
        $('#modalFormulario').modal('show');
        $('#btnEA').click(function (e) {
            if ($('#Codigo').val() == '') {
                alert("rellene el campo Codigo");
                return false;
            }

            if ($('#descripcion').val() == '') {
                alert("rellene el campo Codigo");
                return false;
            }
            if ($('#precio').val() == '') {
                alert("rellene el campo Codigo");
                return false;
            }
            var json = '{"codigo":"' + $('#Codigo').val() + '","idCategoria":"' + $('#selectCategoria').val() + '","Descripcion":"' + $('#descripcion').val() + '","precio":"' + $('#precio').val() + '"}';
            
            $.ajax({
                type: "POST",
                url: "../web1.aspx/AgregarCategoria",
                data: json,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $('#modalFormulario').modal('hide');
                    CargaProductos();




                },
                failure: function (response) {
                    alert(response.d);
                }
            });
            
            
            
        });

    });

    $('#btnBuscar').unbind('click').click(function (e) {
        e.preventDefault();
        $('#buscarModal').modal('show');
        $('#btnB').click(function (e) {
            if ($('#DescriocionBuscar').val() == '') {
                CargaProductos();
                $('#buscarModal').modal('hide');
            } else {
                CargaProductosB();
                $('#buscarModal').modal('hide');
            }
            



        });

    });



});

function CargaCategoria() {

    

    $.ajax({
        type: "POST",
        url: "../web1.aspx/RecuperaCategorias",
      
        contentType: "application/json; charset=utf-8",
        
        success: function (response) {
            $('.selectCategoria').html(response.d).fadeIn()



        },
        failure: function (response) {
            alert(response.d);
        }
    });


}

function CargaProductos() {



    $.ajax({
        type: "POST",
        url: "../web1.aspx/RecuperarProductos",

        contentType: "application/json; charset=utf-8",

        success: function (response) {
            $('#divProductos').html(response.d).fadeIn()

            $('.btnEdit').click(function () {
                $('#modaleditar').modal('show');
                var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                var idCategoria = $(this).parents("tr").find("td")[1].innerHTML;
                var Descripcion = $(this).parents("tr").find("td")[2].innerHTML;
                var Precio = $(this).parents("tr").find("td")[3].innerHTML;
                $('#CodigoE').val(codigo);
                $('#selectCategoriaE').val(idCategoria);
                $('#descripcionE').val(Descripcion);
                $('#precioE').val(Precio);

                $('#btnEditar').unbind('click').click(function () {

                    var json = '{"codigo":"' + $('#CodigoE').val() + '","idCategoria":"' + $('#selectCategoriaE').val() + '","Descripcion":"' + $('#descripcionE').val() + '","precio":"' + $('#precioE').val() + '"}';
                    $.ajax({
                        type: "POST",
                        url: "../web1.aspx/EditarCategoria",
                        data: json,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {

                            $('#modaleditar').modal('hide');
                            CargaProductos();




                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });

                });
            });
            $('.btnEliminar').unbind('click').click(function () {
                if (!confirm("¿Seguro que desea eliminar el producto?")) {
                    return false;
                }
                var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                var idCategoria = $(this).parents("tr").find("td")[1].innerHTML;
                var Descripcion = $(this).parents("tr").find("td")[2].innerHTML;
                var Precio = $(this).parents("tr").find("td")[3].innerHTML;
                var json = '{"codigo":"' + codigo + '","idCategoria":"' + idCategoria + '","Descripcion":"' + Descripcion + '","precio":"' + Precio + '"}';
                $.ajax({
                    type: "POST",
                    url: "../web1.aspx/EliminarCategoria",
                    data: json,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                       
                        CargaProductos();




                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });




}

function CargaProductosB() {

    var jsonn = '{"descripcion":"' + $('#DescriocionBuscar').val() + '"}';
    

    $.ajax({
        type: "POST",
        url: "../web1.aspx/RecuperarProductosB",
        data: jsonn,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#divProductos').html(response.d).fadeIn()

            $('.btnEdit').click(function () {
                $('#modaleditar').modal('show');
                var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                var idCategoria = $(this).parents("tr").find("td")[1].innerHTML;
                var Descripcion = $(this).parents("tr").find("td")[2].innerHTML;
                var Precio = $(this).parents("tr").find("td")[3].innerHTML;
                $('#CodigoE').val(codigo);
                $('#selectCategoriaE').val(idCategoria);
                $('#descripcionE').val(Descripcion);
                $('#precioE').val(Precio);

                $('#btnEditar').unbind('click').click(function () {

                    var json = '{"codigo":"' + $('#CodigoE').val() + '","idCategoria":"' + $('#selectCategoriaE').val() + '","Descripcion":"' + $('#descripcionE').val() + '","precio":"' + $('#precioE').val() + '"}';
                    $.ajax({
                        type: "POST",
                        url: "../web1.aspx/EditarCategoria",
                        data: json,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {

                            $('#modaleditar').modal('hide');
                            CargaProductosB();




                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });

                });
            });
            $('.btnEliminar').unbind('click').click(function () {
                if (!confirm("¿Seguro que desea eliminar el producto?")) {
                    return false;
                }
                var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                var idCategoria = $(this).parents("tr").find("td")[1].innerHTML;
                var Descripcion = $(this).parents("tr").find("td")[2].innerHTML;
                var Precio = $(this).parents("tr").find("td")[3].innerHTML;
                var json = '{"codigo":"' + codigo + '","idCategoria":"' + idCategoria + '","Descripcion":"' + Descripcion + '","precio":"' + Precio + '"}';
                $.ajax({
                    type: "POST",
                    url: "../web1.aspx/EliminarCategoria",
                    data: json,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {


                        CargaProductosB();




                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });


}