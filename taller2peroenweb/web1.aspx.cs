﻿using BOL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace taller2peroenweb
{
    public partial class Formulario_web1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        [System.Web.Services.WebMethod]
        public static string RecuperaCategorias()
        {
            List<categoria> lc = new List<categoria>();

            lc = DAL_Categoria.ListarCategoria();

            string tabla = "";

            foreach (categoria c in lc)
            {
                tabla = tabla + "<option value='";
                tabla = tabla + c.IDCategoria.ToString();

                tabla = tabla + "'>";
                tabla = tabla + c.IDCategoria.ToString();
                tabla = tabla + "-";

                tabla = tabla + c.Descripcion;

                tabla = tabla + "</option>";

            }





            return tabla;
        }

        [System.Web.Services.WebMethod]
        public static string RecuperarProductos()
        {
            List<BOL_Productos> lc = new List<BOL_Productos>();

            lc = DAL_Productos.ListarProductos();

            string tabla = "";
            tabla = tabla + "<table class='table table-hover'><tr>";
            tabla = tabla + "<th>Codigo</th>";
            tabla = tabla + "<th>IdCategoria</th>";
            tabla = tabla + "<th>Descripcion</th>";
            tabla = tabla + "<th>Precio</th>";
            tabla = tabla + "<th>Editar</th>";
            tabla = tabla + "<th>Eliminar</th>";

            tabla = tabla + "</tr>";

            foreach (BOL_Productos c in lc)
            {
                tabla = tabla + "<tr>";
                tabla = tabla + "<td>";
                tabla = tabla + c.codigo.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.IDCategoria.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.Descripcion;

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.precio.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + "<input type=button value='Editar' class='btn  btn-sm btn-info btnEdit'/> ";

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + "<input type=button value='Eliminar' class='btn  btn-sm btn-info btnEliminar'/> ";

                tabla = tabla + "</td>";



                tabla = tabla + "</tr>";
            }
            tabla = tabla + "</table>";





            return tabla;
        }
        [System.Web.Services.WebMethod]
        public static string RecuperarProductosB(String descripcion)
        {
            List<BOL_Productos> lc = new List<BOL_Productos>();

            lc = DAL_Productos.buscarC(descripcion);

            string tabla = "";
            tabla = tabla + "<table class='table table-hover'><tr>";
            tabla = tabla + "<th>Codigo</th>";
            tabla = tabla + "<th>IdCategoria</th>";
            tabla = tabla + "<th>Descripcion</th>";
            tabla = tabla + "<th>Precio</th>";
            tabla = tabla + "<th>Editar</th>";
            tabla = tabla + "<th>Eliminar</th>";

            tabla = tabla + "</tr>";

            foreach (BOL_Productos c in lc)
            {
                tabla = tabla + "<tr>";
                tabla = tabla + "<td>";
                tabla = tabla + c.codigo.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.IDCategoria.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.Descripcion;

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + c.precio.ToString();

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + "<input type=button value='Editar' class='btn  btn-sm btn-info btnEdit'/> ";

                tabla = tabla + "</td>";

                tabla = tabla + "<td>";
                tabla = tabla + "<input type=button value='Eliminar' class='btn  btn-sm btn-info btnEliminar'/> ";

                tabla = tabla + "</td>";



                tabla = tabla + "</tr>";
            }
            tabla = tabla + "</table>";





            return tabla;
        }
        [System.Web.Services.WebMethod]
        public static string EditarCategoria(int codigo,int idCategoria,String Descripcion,int precio)
        {
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                C.codigo = codigo;
                C.Descripcion = Descripcion;
                C.precio = precio;
                C.IDCategoria = idCategoria;
                respuesta = DAL_Productos.ModificarCategoria(C);
                
            }
            catch (Exception exp)
            {
                respuesta = "Error";
            }
            return respuesta;
        }

        [System.Web.Services.WebMethod]
        public static string EliminarCategoria(int codigo, int idCategoria, String Descripcion, int precio)
        {
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                C.codigo = codigo;
                C.IDCategoria = idCategoria;
                C.Descripcion = Descripcion;
                C.precio = precio;



                respuesta = DAL_Productos.EliminarCategoria(C);


                
            }
            catch (Exception exp)
            {

                respuesta = "Error";

            }
            return respuesta;
        }
        [System.Web.Services.WebMethod]
        public static string AgregarCategoria(int codigo, int idCategoria, String Descripcion, int precio)
        {
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                
                C.codigo = codigo;
                C.IDCategoria = idCategoria;
                C.Descripcion = Descripcion;
                C.precio = precio;


                respuesta = DAL_Productos.InsertarProductos(C);


               

            }
            catch (Exception exp)
            {

                respuesta = "error";

            }
            return respuesta;
        }
    }
}