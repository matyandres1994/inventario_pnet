﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BOL;



namespace taller2peroenweb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = DAL_Categoria.ListarCategoria();
            GridView1.DataBind();
            Delete.Visible = false;
            Update.Visible = false;
            
        }
        

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update.Visible = true;
            Delete.Visible = true;
           
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            submit.Visible = true;
            desCat.Visible = true;
            idCat.Visible = true;
            UpdateDes.Visible = false;
            Updating.Visible = false;
        }

        protected void desCat_TextChanged(object sender, EventArgs e)
        {

        }

        protected void idCat_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            categoria C = new categoria();
            string respuesta;
            try
            {
                C.IDCategoria = int.Parse(idCat.Text);
                C.Descripcion = desCat.Text;


                respuesta = DAL_Categoria.InsertarCategorias(C);
                Response.Redirect(Request.RawUrl, true);


            }
            catch (Exception exp)
            {

                Response.Write(exp.Message.ToString());

            }
        }

        protected void Delete_Click1(object sender, EventArgs e)
        {
            submit.Visible = false;
            desCat.Visible = false;
            idCat.Visible = false;

            categoria C = new categoria();
            string respuesta;
            try
            {
                C.IDCategoria = int.Parse(GridView1.SelectedRow.Cells[1].Text);
                C.Descripcion = GridView1.SelectedRow.Cells[2].Text;
                respuesta = DAL_Categoria.EliminarCategoria(C);
                Response.Redirect(Request.RawUrl, true);
            }
            catch(Exception exp)
            {
                Response.Write(exp.Message.ToString());
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            submit.Visible = false;
            desCat.Visible = false;
            idCat.Visible = false;
            Delete.Visible = true;
            Update.Visible = true;
            try
            {
                if (GridView1.SelectedRow.Cells[2].Text == null)
                {

                }
                else
                {
                    UpdateDes.Visible = true;
                    Updating.Visible = true;
                    UpdateDes.Text = GridView1.SelectedRow.Cells[2].Text;
                }
            }
            catch(Exception exp)
            {
                Response.Write(exp.Message.ToString());
            }
        }

        protected void Updated_click(object sender, EventArgs e)
        {
            categoria C = new categoria();
            string respuesta;
            try
            {
              

                C.IDCategoria = int.Parse(GridView1.SelectedRow.Cells[1].Text);
                C.Descripcion = UpdateDes.Text;
                respuesta = DAL_Categoria.ModificarCategoria(C);
                Response.Redirect(Request.RawUrl, true);
            }
            catch (Exception exp)
            {
                Response.Write(exp.Message.ToString());
            }
        }

       
        protected void UpdateDes_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            String x = txtbuscar.Text;
            GridView1.DataSource = DAL_Categoria.buscarC(x);
            GridView1.DataBind();
            Delete.Visible = true;
            Update.Visible = true;
            GridView1.SelectedIndex = 0;
        }
    }
}