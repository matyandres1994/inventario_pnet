﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BOL;

namespace taller2peroenweb
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Delete.Visible = false;
            Update.Visible = false;
            GridView1.DataSource = DAL_Productos.ListarProductos();
            GridView1.DataBind();
            if (!IsPostBack)
            {
                DropDownList1.DataSource = DAL_Categoria.ListarCategoria();
                DropDownList1.DataTextField = "Descripcion";
                DropDownList1.DataValueField = "IDCategoria";
                DropCat.DataSource = DAL_Categoria.ListarCategoria();
                DropCat.DataTextField = "Descripcion";
                DropCat.DataValueField = "IDCategoria";
               
            }
            DropDownList1.DataBind();
            DropCat.DataBind();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update.Visible = true;
            Delete.Visible = true;

        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            DropDownList1.Visible = true;
            Precio.Visible = true;
            IdPro.Visible = true;
            Post.Visible = true;
            Producto.Visible = true;
            Update.Visible = false;
            Delete.Visible = false;
            IdP.Visible = false;
            DropCat.Visible = false;
            Name.Visible = false;
            Price.Visible = false;
            Updated.Visible = false;
        }

        protected void Post_Click(object sender, EventArgs e)
        {
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                System.Diagnostics.Debug.WriteLine($"{DropDownList1.SelectedValue.ToString()}");
                C.codigo = int.Parse(IdPro.Text);
                C.IDCategoria = int.Parse(DropDownList1.SelectedValue.ToString());
                C.Descripcion = Producto.Text;
                C.precio = int.Parse(Precio.Text);


                respuesta = DAL_Productos.InsertarProductos(C);


                Response.Redirect(Request.RawUrl, true);

            }
            catch (Exception exp)
            {

                Response.Write(exp.Message.ToString());

            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                C.codigo = int.Parse(GridView1.SelectedRow.Cells[1].Text);
                C.IDCategoria = int.Parse(GridView1.SelectedRow.Cells[2].Text);
                C.Descripcion = GridView1.SelectedRow.Cells[1].Text;
                C.precio = int.Parse(GridView1.SelectedRow.Cells[1].Text);



                respuesta = DAL_Productos.EliminarCategoria(C);


                Response.Redirect(Request.RawUrl, true);
            }
            catch (Exception exp)
            {

                Response.Write(exp.Message.ToString());

            }
        }

        protected void DropCat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Update.Visible = true;
            Delete.Visible = true;
            IdPro.Visible = false;
            DropDownList1.Visible = false;
            Producto.Visible = false;
            Precio.Visible = false;
            Post.Visible = false;

            try
            {
                if (GridView1.SelectedRow.Cells[1].Text == null)
                {

                }
                else
                {
                    Name.Visible = true;
                    DropCat.Visible = true;
                    Price.Visible = true;
                    Updated.Visible = true;
                    Name.Text = GridView1.SelectedRow.Cells[3].Text;
                    Price.Text = GridView1.SelectedRow.Cells[4].Text;
                    IdP.Text = GridView1.SelectedRow.Cells[1].Text;
                    String valor1 = GridView1.SelectedRow.Cells[2].Text;

                    DropCat.SelectedValue = valor1;

                }
            }
            catch (Exception exp)
            {
                Response.Write(exp.Message.ToString());
            }
        }

        protected void Updated_Click(object sender, EventArgs e)
        {
            
            BOL_Productos C = new BOL_Productos();
            string respuesta;
            try
            {
                C.codigo = int.Parse(IdP.Text);
                C.Descripcion = Name.Text;
                C.precio = int.Parse(Price.Text);
                C.IDCategoria = int.Parse(DropCat.SelectedValue.ToString());
                respuesta = DAL_Productos.ModificarCategoria(C);
                Response.Redirect(Request.RawUrl, true);
            }
            catch (Exception exp)
            {
                Response.Write(exp.Message.ToString());
            }
        }

        protected void btnbuscar_Click(object sender, EventArgs e)
        {
            String x = txtbuscar.Text;
            GridView1.DataSource = DAL_Productos.buscarC(x);
            GridView1.DataBind();
            Delete.Visible = true;
            Update.Visible = true;
            GridView1.SelectedIndex = 0;
        }
    }
}