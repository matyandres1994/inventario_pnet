﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="web1.aspx.cs" Inherits="taller2peroenweb.Formulario_web1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


   <div>
       <h1 class="text-center">Productos</h1>
       <div class="container" id="divProductos"></div>
      

           <button id="btnIngresar" class="btn btn-outline-dark col-12">Ingresar</button>
       <br />
            <button id="btnBuscar" class="btn btn-outline-dark col-12">Buscar</button>
      

       
       
        </div>
    <div id="modalFormulario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<form id="formularioActa" >
						
						<br><br>
						<h4 class="text-center">Ingresar Producto</h4>
						<label>Codigo:</label>
						<input type="number" class="form-control" id="Codigo" placeholder="Codigo" 	name="Codigo">
						<label>Categoria:</label>
                        <select id="selectCategoria" class="form-control selectCategoria">
                            
                        </select>

                        <label>Descripcion:</label>
						<input type="text" class="form-control" id="descripcion" placeholder="Descripcion" 	name="descripcion">
						
                       <label>Precio:</label>
						<input type="number" class="form-control" id="precio" placeholder="Precio" 	name="Precio">
						
						<br>
						<button type="button" id="btnEA" class="btn btn-outline-success form-control">Enviar</button>
                        <br />
                        <br />
					</form>
					
				</div>
			</div>
		</div>
	</div>

    <div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<form id="EditarProductos" >
						
						<br><br>
						<h4 class="text-center">Ingresar Producto</h4>
						<label>Codigo:</label>
						<input type="number" class="form-control" id="CodigoE" placeholder="Codigo" 	name="CodigoE">
						<label>Categoria:</label>
                        <select id="selectCategoriaE" class="form-control selectCategoria">
                            
                        </select>

                        <label>Descripcion:</label>
						<input type="text" class="form-control" id="descripcionE" placeholder="Descripcion" 	name="descripcion">
						
                       <label>Precio:</label>
						<input type="number" class="form-control" id="precioE" placeholder="Precio" 	name="Precio">
						
						<br>
						<button type="button" id="btnEditar" class="btn btn-outline-success form-control btnEditar">Enviar</button>
                        <br />
                        <br />
					</form>
					
				</div>
			</div>
		</div>
	</div>


        <div id="buscarModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<form id="buscarx" >
						
						<br><br>
						<h4 class="text-center">Buscar Producto</h4>
						<label>Descripcion:</label>
						<input type="text" class="form-control" id="DescriocionBuscar" placeholder="Descripcion" 	name="DescriocionBuscar">
						
						<button type="button" id="btnB" class="btn btn-outline-success form-control btnEditar">Enviar</button>
                        <br />
                        <br />
					</form>
					
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="js" runat="server">
     <script src="../Scripts/js/Categorias.js">
        </script>
</asp:Content>
