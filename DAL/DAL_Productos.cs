﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BOL;
using System.Data;
namespace DAL
{
    public class DAL_Productos
    {
        public static List<BOL_Productos> ListarProductos()
        {
            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select * from Productos";



                cmd.Parameters.Clear();
                //cmd.Parameters.AddWithValue("@id_empresa", id_empresa);
                //cmd.Parameters.AddWithValue("@id_proceso", id_proceso);
                //cmd.Parameters.AddWithValue("@fecha_programacion", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 23:59:59")));
                //cmd.Parameters.AddWithValue("@fecha_programacion_initial", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 00:00:01")));
                //cmd.Parameters.AddWithValue("@nivel", nivel);
                //cmd.Parameters.AddWithValue("@id_user", id_user);
                SqlDataReader rdr = cmd.ExecuteReader();

                List<BOL_Productos> lp = new List<BOL_Productos>();
                while (rdr.Read())
                {
                    lp.Add(new BOL_Productos
                    {
                        codigo = int.Parse(rdr["codigo"].ToString()),
                        IDCategoria = int.Parse(rdr["IDCategoria"].ToString()),
                        Descripcion = rdr["Descripcion"].ToString(),
                        precio = int.Parse(rdr["precio"].ToString()),

                    });
                }
                cmd.Connection.Close();
                cmd.Dispose();
                return lp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string ModificarCategoria(BOL_Productos P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "modificarproductos";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@codigo", P.codigo);
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);
                cmd.Parameters.AddWithValue("@precio", P.precio);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }
        public static string InsertarProductos(BOL_Productos P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "insertarproductos";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@codigo", P.codigo);
                
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);
                cmd.Parameters.AddWithValue("@precio", P.precio);


                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }

        public static List<BOL_Productos> buscarC(String P)
        {

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select * from Productos where Descripcion='" + P + "'";



                cmd.Parameters.Clear();
                //cmd.Parameters.AddWithValue("@id_empresa", id_empresa);
                //cmd.Parameters.AddWithValue("@id_proceso", id_proceso);
                //cmd.Parameters.AddWithValue("@fecha_programacion", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 23:59:59")));
                //cmd.Parameters.AddWithValue("@fecha_programacion_initial", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 00:00:01")));
                //cmd.Parameters.AddWithValue("@nivel", nivel);
                //cmd.Parameters.AddWithValue("@id_user", id_user);
                SqlDataReader rdr = cmd.ExecuteReader();

                List<BOL_Productos> lp = new List<BOL_Productos>();
                while (rdr.Read())
                {
                    lp.Add(new BOL_Productos
                    {
                        codigo = int.Parse(rdr["codigo"].ToString()),
                        IDCategoria = int.Parse(rdr["IDCategoria"].ToString()),
                        Descripcion = rdr["Descripcion"].ToString(),
                        precio = int.Parse(rdr["precio"].ToString()),

                    });
                }
                cmd.Connection.Close();
                cmd.Dispose();
                return lp;
            }
            catch (Exception)
            {
                throw;
            }

        }


        public static string EliminarCategoria(BOL_Productos P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "eliminarproductos";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@codigo", P.codigo);
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);
                cmd.Parameters.AddWithValue("@precio", P.precio);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }
    }
}
