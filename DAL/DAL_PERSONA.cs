﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BOL;
using System.Data;

namespace DAL
{
    public class DAL_Persona
    {

     




        public static List<Persona> ListarPersonas()
        {
            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select * from Categoria";



                cmd.Parameters.Clear();
                //cmd.Parameters.AddWithValue("@id_empresa", id_empresa);
                //cmd.Parameters.AddWithValue("@id_proceso", id_proceso);
                //cmd.Parameters.AddWithValue("@fecha_programacion", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 23:59:59")));
                //cmd.Parameters.AddWithValue("@fecha_programacion_initial", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 00:00:01")));
                //cmd.Parameters.AddWithValue("@nivel", nivel);
                //cmd.Parameters.AddWithValue("@id_user", id_user);
                SqlDataReader rdr = cmd.ExecuteReader();

                List<Persona> lp = new List<Persona>();
                while (rdr.Read())
                {
                    lp.Add(new Persona
                    {
                        rut = rdr["rut"].ToString(),
                        nombre = rdr["nombre"].ToString()

                    });
                }
                cmd.Connection.Close();
                cmd.Dispose();
                return lp;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public static string InsertarPersona(Persona P)
        {

        string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
             
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "Insertar_Persona";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@rut", P.rut);
                cmd.Parameters.AddWithValue("@nombre", P.nombre);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";
    
            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }

        public static string ModificarPersona(Persona P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "ModificarPersonas";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@rut", P.rut);
                cmd.Parameters.AddWithValue("@nombre", P.nombre);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }





    }
}
