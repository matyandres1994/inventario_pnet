﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BOL;
using System.Data;

namespace DAL { 
    public class DAL_Categoria
    {
        public static List<categoria> ListarCategoria()
        {
            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select * from Categorias";



                cmd.Parameters.Clear();
                //cmd.Parameters.AddWithValue("@id_empresa", id_empresa);
                //cmd.Parameters.AddWithValue("@id_proceso", id_proceso);
                //cmd.Parameters.AddWithValue("@fecha_programacion", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 23:59:59")));
                //cmd.Parameters.AddWithValue("@fecha_programacion_initial", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 00:00:01")));
                //cmd.Parameters.AddWithValue("@nivel", nivel);
                //cmd.Parameters.AddWithValue("@id_user", id_user);
                SqlDataReader rdr = cmd.ExecuteReader();

                List<categoria> lp = new List<categoria>();
                while (rdr.Read())
                {
                    lp.Add(new categoria
                    {
                        IDCategoria = int.Parse(rdr["IDCategoria"].ToString()),
                        Descripcion = rdr["Descripcion"].ToString()

                    });
                }
                cmd.Connection.Close();
                cmd.Dispose();
                return lp;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<categoria> buscarC(String P)
        {
            
                try
                {



                    string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                    SqlCommand cmd = new SqlCommand();


                    cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "Select * from Categorias where Descripcion='"+P+"'";



                cmd.Parameters.Clear();
                
                //cmd.Parameters.AddWithValue("@id_proceso", id_proceso);
                //cmd.Parameters.AddWithValue("@fecha_programacion", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 23:59:59")));
                //cmd.Parameters.AddWithValue("@fecha_programacion_initial", Convert.ToDateTime(fecha_programacion.ToString("dd-MM-yyyy 00:00:01")));
                //cmd.Parameters.AddWithValue("@nivel", nivel);
                //cmd.Parameters.AddWithValue("@id_user", id_user);
                SqlDataReader rdr = cmd.ExecuteReader();

                    List<categoria> lp = new List<categoria>();
                    while (rdr.Read())
                    {
                        lp.Add(new categoria
                        {
                            IDCategoria = int.Parse(rdr["IDCategoria"].ToString()),
                            Descripcion = rdr["Descripcion"].ToString()

                        });
                    }
                    cmd.Connection.Close();
                    cmd.Dispose();
                    return lp;
                }
                catch (Exception)
                {
                    throw;
                }
            
        }

        public static string InsertarCategorias(categoria P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "Insertar_Categorias";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }

        public static string ModificarCategoria(categoria P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "modificarcategoria";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }
        public static string EliminarCategoria(categoria P)
        {

            string respuesta = "";

            try
            {



                string strConnString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                SqlCommand cmd = new SqlCommand();


                cmd.Connection = new SqlConnection(strConnString);
                cmd.Connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "eliminarcategoria";



                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@IDCategoria", P.IDCategoria);
                cmd.Parameters.AddWithValue("@Descripcion", P.Descripcion);

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
                cmd.Dispose();

                respuesta = "ok";

            }
            catch (SqlException exp)
            {
                respuesta = exp.Message.ToString();
            }



            return respuesta;
        }
    }


}