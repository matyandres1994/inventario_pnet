﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class BOL_Productos
    {
        public int codigo { get; set; }
        public int IDCategoria { get; set; }
        public String Descripcion { get; set; }
        public int precio { get; set; }
    }
}
